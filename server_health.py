#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import logging
import sleekxmpp
import os
import time
import psutil

logging.basicConfig(filename='health.log', level=logging.INFO)


class SendMsgBot(sleekxmpp.ClientXMPP):
    """
    A basic Sleekxmpp bot that will log in, send a message,
    and then log out.
    """

    def __init__(self, jid, password, recipient, message):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)

        # The message we wish to send, and the JID that
        # will receive it.
        self.recipient = recipient
        self.msg = message
        self.high_load = 0.0
        self.previous_load_maximum = 0.0

        self.mem_string = "Not set yet"
        self.memory = "Not set yet"
        self.load_string = "Not set yet"
        self.load = "Not set yet"
        self.mem_string = "Not set yet"
        self.load_maximum = "Not set yet"
        self.cpu_status = "Not set yet"

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can initialize
        # our roster.
        self.add_event_handler("session_start", self.start)
        self.add_event_handler("message", self.message)

    def set_load_parameters(self, high_load, previous_load_maximum):
        self.high_load = high_load
        self.previous_load_maximum = previous_load_maximum

    def health(self, mem_string, memory, load_string, load, load_maximum, cpu_status, load_string_status):
        self.mem_string = mem_string
        self.memory = memory
        self.load_string = load_string
        self.load = load
        self.mem_string = mem_string
        self.load_maximum = load_maximum
        self.cpu_status = cpu_status
        self.cpu_status = 'CPUS : ' + str(self.cpu_status)
        self.load_string_status = load_string_status
      # cpu_process = get_process()

    def start(self, event):
        """
        Process the session_start event.

        Typical actions for the session_start event are
        requesting the roster and broadcasting an initial
        presence stanza.

        Arguments:
            event -- An empty dictionary. The session_start
                     event does not provide any additional
                     data.
                     :param event:
                     :param event:
        """
        self.send_presence()
        self.get_roster()

        self.send_message(mto=self.recipient,
                          mbody=self.msg,
                          mtype='chat')

        # self.disconnect()

    def send_msg(self, msg):
        self.send_message(mto=self.recipient,
                          mbody=msg,
                          mtype='chat')  # normal or chat

    def set_status(self, status, status_msg):
        self.send_presence(pstatus=status_msg, pshow=status)

    def send_status(self):
        print("TODO")
        msg = 'TODO'
        self.send_message(mto=self.recipient,
                          mbody=msg,
                          mtype='chat')  # normal or chat

    def message(self, msg):
        """
        Process incoming message stanzas. Be aware that this also
        includes MUC messages and error messages. It is usually
        a good idea to check the messages's type before processing
        or sending replies.

        Arguments:
            msg -- The received message stanza. See the documentation
                   for stanza objects and the Message stanza to see
                   how it may be used.
                   :param msg:
                   :param msg:
        """
        if msg['type'] in ('chat', 'normal'):
            msg_content = '\n' + self.load_string + '\n' + self.cpu_status + '\n' + self.mem_string
            self.send_message(mto=msg['from'], mbody=msg_content, mtype='chat')  # normal or chat
            if "How you doin' ?" in self.load_string_status:
                s = 'available'
            else:
                s = 'dnd'
            self.set_status(s, self.load_string_status)

class Health:
    def __init__(self):
        self.high_load = 1.2
        self.previous_load_maximum = 0.0
        self.xmpp = None

    def to_gib(self, bytes, factor=2 ** 30, suffix="GiB"):
        """ Converti un nombre d'octets en gibioctets.

            Ex : 1073741824 octets = 1073741824/2**30 = 1GiO
            :param factor:
            :param bytes:
            :param suffix:
            :param bytes:
        """
        return "%0.2f%s" % (bytes / factor, suffix)

    def get_memory(self):
        results = {}
        memory = psutil.virtual_memory()
        results['memory'] = '{used}/{total} ({percent}%)'.format(
            used=self.to_gib(memory.active),
            total=self.to_gib(memory.total),
            percent=memory.percent,
            available=memory.available
        )
        mem_usage = results['memory']
        mem_string = "Memory (Go):  %s" % mem_usage
        return mem_string, memory

    def get_cpu(self):
        results = {'cpus': tuple("%s%%" % x for x in psutil.cpu_percent(percpu=True))}
        return results['cpus']

    def get_load(self):
        load = os.getloadavg()
        load_string = "Load: %s %s %s" % load
        return load_string, load

    def read_parameters(self, file):
        with open(file, "r") as f:
            parameters = []
            for line in f:
                line = line.rstrip('\r\n')
                parameters.append(line)
            jid = None
            password = None
            to = None
            jid = parameters[0]
            password = parameters[1]
            to = parameters[2]
            return jid, password, to

    def read_filetext(self, file):
        with open(file, "r") as f:
            text = f.read()
        return text

    def get_health_and_set_status(self):
        # Determine the load averages and the maximum of the 1, 10 and 15 minute
        # averages.
        # In case the maximum is above the "high_load" value, the status will be
        # set to "do not disturb" with the load averages as status message.
        # The status will be updated continuously once we reached high load.
        # When the load decreases and the maximum is below the "high_load" value
        # again, the status is reset to "available".
        mem_string, memory = self.get_memory()
        load_string, load = self.get_load()
        load_string += " "
        load_string += mem_string
        update_status = False
        load_maximum = max(list(load))
        cpu_status = self.get_cpu()

        load_string_status = "How you doin' ? " + load_string
        if load_maximum >= self.high_load:
            load_string_status = "It's hot in here! " + load_string
        if load_maximum >= 1.25 * self.high_load:
            load_string_status = "I am smoking! " + load_string
        if load_maximum >= 2 * self.high_load:
            load_string_status = "I am melting! " + load_string
        if load_maximum >= 2.5 * self.high_load:
            load_string_status = "Kill me please! " + load_string
        if load_maximum >= self.high_load:
            self.xmpp.set_status('dnd', load_string_status)
            update_status = True
        elif load_maximum < self.high_load <= self.previous_load_maximum:
            load_string_status = "How you doin' ? " + load_string
            self.xmpp.set_status('available', load_string_status)
            update_status = True

        threshold = 0.1 * 1024 * 1024  # 100MB
        if memory.available < threshold:
            load_string_status = "I am out of memory! " + load_string
            self.xmpp.set_status('dnd', load_string_status)
            update_status = True
        if update_status is True:
            self.xmpp.send_msg(load_string_status)
        self.xmpp.health(mem_string, memory, load_string, load, load_maximum, cpu_status, load_string_status)
        return load_string, mem_string, load_maximum

    def handle_text(self, text):
        file = '/home/arnaud/.config/xmpp/send_xmpprc'
        file = 'send_xmpprc'
        jid, password, to = self.read_parameters(file)
        jid += '/health'
        logging.info("Start")
        #    print(jid,password, to)
        if jid is None or password is None or to is None:
            print("Error in config file")
            logging.error("Error in config file")
            return 1
        self.xmpp = SendMsgBot(jid, password, to, text)
        self.xmpp.register_plugin('xep_0030')  # Service Discovery
        self.xmpp.register_plugin('xep_0199')  # XMPP Ping
        self.xmpp.connect()
        if self.xmpp.connect():
            # block=True will block the current thread. By default, block=False
            self.xmpp.process()
        else:
            print("Unable to connect.")
        high_load = 1.2
        previous_load_maximum = 0
        self.xmpp.send_msg("Agayon.be en ligne")
        time.sleep(3)
        load_string, load = self.get_load()
        mem_string, memory = self.get_memory()
        load_string += " " + mem_string
        load_string_status = "How you doin' ? " + load_string
        self.xmpp.send_msg(load_string_status)
        self.xmpp.set_status('available', load_string_status)
        while True:
            load_string, mem_string, load_maximum = self.get_health_and_set_status()
            logging.debug("Load : " + load_string)
            logging.debug("Memory : " + mem_string)
            self.previous_load_maximum = load_maximum
            time.sleep(10)

    # def get_process():
    #     process_string = ""
    #     capturedCpu_pslist = [ (process, process.get_cpu_percent() ) for process in psutil.get_process_list() ]
    #     pslist = psutil.get_process_list()
    #     consuming_pslist = filter( filterCpu, pslist )
    #     for process in consuming_pslist:
    #         process_string += process.name + " " + str( process.get_cpu_percent() )
    #     return process_string


if __name__ == "__main__":
    text = ''
    h = Health()

    #    text = "" + '\n'+ read_filetext('/tmp/logwatch.report')
    #    print(text)
    h.handle_text(text)
