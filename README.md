The aim of this project is to communicate with a XMPP bot. It can give informations about the load of the server, the CPU's and the memory. When you send it a 
message, it reply with useful information:

```
[06:30:00 PM] ‎jnanar‎: test
[06:30:00 PM] ‎server‎:
Load: 0.0 0.01 0.05
CPUS : ('0.6%', '0.6%')
Memory (Go):  0.87GiB/1.96GiB (20.9%)
```

Note: the memory usage is based on these values:
```
Memory (Go): used/total (percent)
```
where percent accounts for the shared memory and the cache.

You need to create a simple config file containing the following informations:

```
jid of the server
password
recipient
```
where

  * jid of the server is an account created for this usage.
  * password is the the associated password of the account
  * recipient is your jid


For now, the file need to be named ```send_xmpprc``` and it need to be placed in the same folder than the script. 

